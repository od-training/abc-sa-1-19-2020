import {
  Component,
  Input,
  OnChanges,
  SimpleChange,
  SimpleChanges
} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

const URLPREFIX = 'https://www.youtube.com/embed';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnChanges {
  @Input() videoId?: string | null;

  youtubeUrl: SafeResourceUrl | null = null;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnChanges(changes: SimpleChanges) {
    const newId = changes['videoId'].currentValue;
    if (!newId) {
      this.youtubeUrl = null;
    }

    if (!URLPREFIX.includes('youtube')) {
      this.youtubeUrl = null;
      return;
    }

    this.youtubeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      `${URLPREFIX}/${newId}`
    );
  }
}
