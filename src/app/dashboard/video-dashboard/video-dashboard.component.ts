import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Video } from '../../types';
import { VideoDataService } from '../../services/video-data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {
  videoList: Observable<Video[]>;
  currentVideoId: Observable<string | null>;

  constructor(vds: VideoDataService, router: Router, route: ActivatedRoute) {
    this.videoList = vds.videos.pipe(
      tap(videos => {
        const queryParams = route.snapshot.queryParamMap;
        if (!queryParams.get('selectedVideo')) {
          router.navigate([], {
            queryParams: {
              selectedVideo: videos[0].id
            }
          });
        }
      })
    );

    this.currentVideoId = vds.getSelectedVideoId();
  }
}
