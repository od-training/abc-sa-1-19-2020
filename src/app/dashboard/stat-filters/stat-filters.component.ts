import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { startWith, debounceTime } from 'rxjs/operators';
import { VideoDataService } from 'src/app/services/video-data.service';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent {
  statFiltersForm: FormGroup;
  titleSearch = new FormControl('');

  regionOptions = [
    { label: 'west', value: 'West' },
    { label: 'north', value: 'North' },
    { label: 'east', value: 'East' },
    { label: 'south', value: 'South' }
  ];

  constructor(vds: VideoDataService) {
    this.statFiltersForm = new FormGroup({
      age: new FormControl(20),
      region: new FormControl('West'),
      date: new FormControl()
    });

    // const date = this.statFiltersForm.get('date');

    // if (date) {
    //   date.valueChanges.subscribe(formData => console.log(formData));
    // }

    // this.statFiltersForm.valueChanges
    //   .pipe(startWith(this.statFiltersForm.value), debounceTime(200))
    //   .subscribe(formData => console.log(formData));

    this.titleSearch.valueChanges.subscribe(title =>
      vds.lastChangedValue(title)
    );
  }
}
