import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { Video } from '../..//types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {
  @Input() videos?: Video[];
  @Input() selectedVideo?: string | null;

  constructor(private router: Router) {}

  selectVideo(video: Video) {
    let newSelection: string | null;
    if (video.id === this.selectedVideo) {
      newSelection = null;
    } else {
      newSelection = video.id;
    }

    this.router.navigate([], {
      queryParams: {
        selectedVideo: newSelection
      }
    });
  }
}
