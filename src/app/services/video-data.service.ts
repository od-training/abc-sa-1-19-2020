import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Video } from '../types';
import { map, switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  videos: Observable<Video[]>;
  filterStream = new BehaviorSubject<string>('');

  constructor(private http: HttpClient, private route: ActivatedRoute) {
    this.videos = this.getVideos().pipe(
      switchMap(videoData =>
        this.filterStream
          .asObservable()
          .pipe(
            map(title =>
              videoData.filter(video =>
                video.title.toUpperCase().startsWith(title.toUpperCase())
              )
            )
          )
      )
    );
  }

  lastChangedValue(value: string) {
    this.filterStream.next(value);
  }

  getVideos() {
    return this.http.get<Video[]>('https://api.angularbootcamp.com/videos');
  }

  getTransformedVideos() {
    return this.getVideos().pipe(map(transformVideos));
  }

  getSelectedVideoId() {
    return this.route.queryParamMap.pipe(
      map(params => params.get('selectedVideo'))
    );
  }
}

function transformVideos(videos: Video[]) {
  return videos.map(video => {
    return { ...video, title: video.title.toUpperCase() };
  });
}
